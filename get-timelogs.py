import json
import os
import sys

from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport


def query_timelogs(username, projectId, startDate, endDate):
    bearer_token = os.environ.get("GRAPHQL_TOKEN", "")
    transport = RequestsHTTPTransport(
        url="https://gitlab.com/api/graphql",
        verify=True,
        retries=3,
        headers={"Authorization": f"Bearer {bearer_token}"},
    )

    client = Client(transport=transport, fetch_schema_from_transport=True)

    with open("timelog-query.graphql", "r") as file:
        query_template = file.read()

    query = gql(query_template)

    params = {
        "username": username,
        "projectId": projectId,
        "startDate": startDate,
        "endDate": endDate,
    }

    result = client.execute(query, variable_values=params)

    if result["timelogs"]["pageInfo"]["hasNextPage"]:
        print("Unexpected condition: There is another page. Do something")
        sys.exit(1)

    return result


def print_result(result):
    total_spent_seconds = 0
    aggregates = {"issue": {}, "mergeRequest": {}}

    print("## Time records")
    print()

    print("| user | date | type | iid | time |")
    print("| ---- | ---- | ---- | --- | ---- |")

    for node in result["timelogs"]["nodes"]:
        entry_type = "issue" if node["issue"] is not None else "mergeRequest"
        entry_type_label = "Issue" if node["issue"] is not None else "MergeRequest"
        entry = node["issue"] or node["mergeRequest"]
        iid = entry["iid"]
        time_spent_seconds = node["timeSpent"]
        total_spent_seconds += time_spent_seconds
        time_spent_minutes = time_spent_seconds / 60

        print(
            f'| {node["user"]["username"]} | {node["spentAt"]} | {entry_type_label} | {iid} | {time_spent_minutes}m |'
        )

        entry_aggregate = aggregates[entry_type].get(
            iid, {"title": entry["title"], "sumTimeSpentSeconds": 0}
        )
        old_sum = entry_aggregate["sumTimeSpentSeconds"]
        new_sum = old_sum + time_spent_seconds
        new_entry_aggregate = {"title": entry["title"], "sumTimeSpentSeconds": new_sum}
        aggregates[entry_type][iid] = new_entry_aggregate

    print()
    print("## Issues")
    print()
    print_aggregate(aggregates["issue"])

    print()
    print("## Merge Requests")
    print()
    print_aggregate(aggregates["mergeRequest"])

    total_spent_minutes = total_spent_seconds / 60.0
    total_spent_hours = total_spent_minutes / 60.0

    print()
    print("## Total")
    print()
    print(f"{total_spent_hours:.2f}h")


def print_aggregate(aggregate):
    print("| iid | title | spent |")
    print("| --- | ----- | ----- |")
    print()

    for iid in aggregate.keys():
        issue_or_mr = aggregate[iid]
        sumTimeSpentMinutes = issue_or_mr["sumTimeSpentSeconds"] / 60.0
        sumTimeSpentHours = sumTimeSpentMinutes / 60.0
        print(f'| {iid} | {issue_or_mr["title"]} | {sumTimeSpentHours:.2f}h |')


def read_query_variables():
    with open("query-variables.json", "r") as variables_file:
        query_variables = json.load(variables_file)
        return query_variables


if __name__ == "__main__":
    query_variables = read_query_variables()
    result = query_timelogs(**query_variables)
    assert result is not None
    print_result(result)
