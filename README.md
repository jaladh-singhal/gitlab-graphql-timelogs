# Custom GitLab time reports using GraphQL

## Instructions

1. Install Pipenv (see [instructions](https://pipenv.pypa.io/en/latest/index.html#install-pipenv-today))
2. Use Pipenv to install dependencies: `pipenv install`
3. Copy `query-variables.json.example` to `query-variables.json`, and replace username, projectID, startDate, and endDate variables with your chosen ones
4. Copy `.env.example` to `.env` and replace `glpat-XXXXXXXXXX-XXXXXXXXX` with your own GitLab token
5. Run the main script: `pipenv run main`

## TODO

- [ ] Add option to export the time logs as CSV format
- [ ] Implement and document some [Python best practices](https://sourcery.ai/blog/python-best-practices/)
- [ ] Better documentation
